package app.com.toasterlibrary;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class ToasterMessage {

    public static void s(Context c, String message){

        Toast.makeText(c,message,Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(c,UnityPlayerActivity.class);
        c.startActivity(intent);

    }
}
